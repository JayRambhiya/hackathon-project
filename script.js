new WOW().init();


$(document).ready(function(){
  $("#features-list").owlCarousel({
      items:5,
      margin:20,
      smartSpeed:700,
      autoplayHoverPause:true,
      loop:true,
      dots:false,
      mouseDrag:false,
      touchDrag:false,
      center:true,
  });
});


$(document).ready(function(){
  $("#test").owlCarousel({
      items:2,
      autoplay:false,
      margin:0,
      nav:true,
      loop:true,
      smartSpeed:700,
      autoplayHoverPause:true,
      dots:false
  });
});


$(document).ready(function(){
  $("#client-list").owlCarousel({
      items:6,
      autoplay:false,
      margin:20,
      loop:true,
      nav:false,
      smartSpeed:700,
      autoplayHoverPause:true,
      dots:false,
      navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
  });
});


$(document).ready(function(){
    $("#archi-list").owlCarousel({
        items:4,
        autoplay:false,
        margin:20,
        loop:true,
        nav:false,
        smartSpeed:700,
        autoplayHoverPause:true,
        dots:false,
        navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
    });
  });
  



